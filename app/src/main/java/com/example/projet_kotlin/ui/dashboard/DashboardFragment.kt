package com.example.projet_kotlin.ui.dashboard

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.projet_kotlin.R
import com.example.projet_kotlin.Tour
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File
import java.io.IOException
import java.nio.file.Paths


class DashboardFragment : Fragment() {

    private lateinit var listView: ListView
    private var jsonFileString: String? = "";

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val preferences: SharedPreferences = requireActivity().application.getSharedPreferences("TOURS", Context.MODE_PRIVATE)

        val gson = Gson()
        val listPersonType = object : TypeToken<List<Tour>>() {}.type
        var tours: List<Tour>

        if (!preferences.contains("toursString")) {
            jsonFileString = getJsonDataFromAsset(requireActivity().application, "tours.json")
            tours = gson.fromJson(jsonFileString, listPersonType)
        }
        else
            tours = gson.fromJson(preferences.getString("toursString", getJsonDataFromAsset(requireActivity().application, "tours.json")), listPersonType)

        var rootView = inflater.inflate(R.layout.fragment_dashboard, container, false)

        listView = rootView.findViewById(R.id.tours_list_view)

        val listItems = arrayOfNulls<String>(tours.size)

        for (i in tours.indices) {
            val recipe = tours[i]
            listItems[i] = recipe.name + " - " + if(recipe.hasLearnt) "Appris" else "Pas appris"
        }

        val adapter = ArrayAdapter(
            requireActivity().application,
            android.R.layout.simple_list_item_1,
            listItems
        )
        listView.adapter = adapter

        listView.setOnItemClickListener { parent, view, position, id ->
            var tv = view as TextView

            for (i in tours.indices) {
                var recipe = tours[i]
                if ("Pas appris" in tv.text) {
                    if (recipe.name in tv.text) {
                        recipe.hasLearnt = true;
                        tv.text = recipe.name + " - " + "Appris";
                    }
                }
                else if ("Appris" in tv.text){
                    if (recipe.name in tv.text) {
                        recipe.hasLearnt = false;
                        tv.text = recipe.name + " - " + "Pas appris"
                    }
                }
            }
            val jsonString = gson.toJson(tours)
            preferences.edit().putString("toursString", jsonString).apply()
        }

        return rootView
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}